package com.scale360.assignment.taskservices.dao.impl;

import java.io.Serializable;

import com.scale360.assignment.taskservices.dao.TaskDAO;
import com.scale360.assignment.taskservices.persistence.entities.Task;

public class TaskDAOImpl extends GenericDAOImpl<Task, Serializable> implements TaskDAO {

}
