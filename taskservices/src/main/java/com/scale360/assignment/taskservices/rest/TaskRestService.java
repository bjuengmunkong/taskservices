package com.scale360.assignment.taskservices.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.jboss.resteasy.spi.BadRequestException;

import com.scale360.assignment.taskservices.business.TaskBusinessAction;
import com.scale360.assignment.taskservices.business.impl.TaskBusinessActionImpl;
import com.scale360.assignment.taskservices.model.TaskModel;

@Path("/")
public class TaskRestService {
	private static Logger logger = Logger.getLogger(TaskRestService.class);
	
	private TaskBusinessAction taskBusinessAction = new TaskBusinessActionImpl();

	@GET
	@Path("/tasks")
	@Wrapped(element = "tasks")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<TaskModel> getTasks() throws Exception {
		logger.info("Start: getTasks()");
		List<TaskModel> tasks = taskBusinessAction.getTasks();

		logger.info("Finish: getTasks()");
		return tasks;
	}

	@GET
	@Path("/task/{taskId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public TaskModel getTaskByTaskId(@PathParam("taskId") Integer taskId) throws Exception {
		logger.info("Start: getTaskByTaskId()");
		TaskModel task = taskBusinessAction.getTaskByTaskId(taskId);
		logger.info("Finish: getTaskByTaskId()");
		return task;
	}

	@PUT
	@Path("/task/{taskId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public TaskModel updateTaskStatusByTaskId(@PathParam("taskId") Integer taskId, @QueryParam("status") String status)
			throws Exception {
		logger.info("Start: updateTaskStatusByTaskId()");

		// validate status
		if(status == null){
			logger.error("the status param is missing");
			throw new BadRequestException("the status param is missing");
		}
		if (!(TaskModel.StatusEnum.done.toString().equals(status)
				|| TaskModel.StatusEnum.pending.toString().equals(status))) {
			logger.error("the status param is invalid");
			throw new BadRequestException("the status param is invalid");
		}

		TaskModel task = taskBusinessAction.updateTaskStatusByTaskId(taskId, status);

		logger.info("Finish: updateTaskStatusByTaskId()");
		return task;
	}

	@DELETE
	@Path("/task/{taskId}")
	public Response deleteTaskByTaskId(@PathParam("taskId") Integer taskId) throws Exception {
		logger.info("Start: deleteTaskByTaskId()");
		taskBusinessAction.deleteTaskByTaskId(taskId);
		
		logger.info("Finish: deleteTaskByTaskId()");
		return Response.ok("task deleted").build();
	}

	@PUT
	@Path("/task")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public TaskModel updateTask(final TaskModel t) throws Exception {
		logger.info("Start: updateTask()");

		// validate task request body
		if(t == null){
			logger.error("the task param is missing");
			throw new BadRequestException("the task param is missing");
		}
		if(Integer.valueOf(0).equals(t.getId())){
			logger.error("the task param is invalid");
			throw new BadRequestException("the task param is invalid");
		}

		TaskModel task = taskBusinessAction.updateTask(t);

		logger.info("Finish: updateTask()");
		return task;
	}

	@POST
	@Path("/task")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public TaskModel createTask(final TaskModel t) throws Exception {
		logger.info("Start: createTask()");

		// validate task request body
		if(t == null){
			logger.error("the task param is missing");
			throw new BadRequestException("the task param is missing");
		}
		if(!Integer.valueOf(0).equals(t.getId())){
			logger.error("the task param is invalid");
			throw new BadRequestException("the task param is invalid");
		}

		TaskModel task = taskBusinessAction.createTask(t);

		logger.info("Finish: createTask()");
		return task;
	}

	@OPTIONS
	@Path("/{path:.*}")
	public Response handleCORSRequest(@HeaderParam("Access-Control-Request-Method") final String requestMethod,
			@HeaderParam("Access-Control-Request-Headers") final String requestHeaders) {
		final ResponseBuilder retValue = Response.ok();
		return retValue.build();
	}

}
