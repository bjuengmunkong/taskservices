package com.scale360.assignment.taskservices.dao;

import com.scale360.assignment.taskservices.dao.impl.TaskDAOImpl;

public class DAOFactory {
	/**
	 * Gets Task DAO
	 * 
	 * @return
	 */
	public static TaskDAO getTaskDAO() {
		return new TaskDAOImpl();
	}
	
}
