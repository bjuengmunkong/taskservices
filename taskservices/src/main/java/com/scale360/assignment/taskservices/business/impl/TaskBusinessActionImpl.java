package com.scale360.assignment.taskservices.business.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jboss.resteasy.spi.BadRequestException;

import com.scale360.assignment.taskservices.business.TaskBusinessAction;
import com.scale360.assignment.taskservices.dao.DAOFactory;
import com.scale360.assignment.taskservices.dao.TaskDAO;
import com.scale360.assignment.taskservices.model.TaskModel;
import com.scale360.assignment.taskservices.persistence.entities.Task;

public class TaskBusinessActionImpl implements TaskBusinessAction {
	private static Logger logger = Logger.getLogger(TaskBusinessActionImpl.class);
	private TaskDAO dao = DAOFactory.getTaskDAO();

	@Override
	public List<TaskModel> getTasks() throws Exception {
		List<TaskModel> taskList = new ArrayList<>();
		List<Task> tasks = dao.findAll();
		for (Task t : tasks) {
			taskList.add(parseTask(t));
		}
		return taskList;
	}

	@Override
	public TaskModel getTaskByTaskId(Integer taskId) throws Exception {
		Task t = dao.find(taskId);
		if (t == null) {
			logger.error("taskId is invallid");
			throw new BadRequestException("taskId is invallid");
		}
		return parseTask(t);
	}

	@Override
	public TaskModel updateTaskStatusByTaskId(Integer taskId, String status) throws Exception {
		Task t = dao.find(taskId);
		if (t == null) {
			logger.error("taskId is invallid");
			throw new BadRequestException("taskId is invallid");
		}
		t.setStatus(status);
		Task t2 = dao.update(t);
		return parseTask(t2);
	}

	@Override
	public void deleteTaskByTaskId(Integer taskId) throws Exception {
		Task t = dao.find(taskId);
		if (t == null) {
			logger.error("taskId is invallid");
			throw new BadRequestException("taskId is invallid");
		}
		dao.delete(taskId);
	}

	@Override
	public TaskModel updateTask(TaskModel taskModel) throws Exception {
		Task t = dao.find(taskModel.getId());
		if (t == null) {
			logger.error("taskId is invallid");
			throw new BadRequestException("taskId is invallid");
		}
		t.setSubject(taskModel.getSubject());
		t.setDetail(taskModel.getDetail());
		t.setStatus(taskModel.getStatus().toString());
		Task t2 = dao.update(t);
		return parseTask(t2);
	}

	@Override
	public TaskModel createTask(TaskModel taskModel) throws Exception {
		Task t = new Task();
		t.setSubject(taskModel.getSubject());
		t.setDetail(taskModel.getDetail());
		t.setStatus(taskModel.getStatus().toString());
		Task t2 = dao.save(t);
		return parseTask(t2);
	}

	/**
	 * Parse task entity to model
	 * 
	 * @param t
	 * @return
	 */
	private TaskModel parseTask(Task t) {
		TaskModel.StatusEnum enumStatus = TaskModel.StatusEnum.pending.toString().equals(t.getStatus())
				? TaskModel.StatusEnum.pending : TaskModel.StatusEnum.done;
		TaskModel task = new TaskModel(t.getId(), t.getSubject(), t.getDetail(), enumStatus);
		return task;
	}

}
