package com.scale360.assignment.taskservices.model;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonPropertyOrder({ "id", "subject", "detail", "status" })
public class TaskModel {

	@XmlElement(name = "id")
	@JsonProperty("id")
	private Integer id = null;

	@XmlElement(name = "subject")
	@JsonProperty("subject")
	private String subject = null;

	@XmlElement(name = "detail")
	@JsonProperty("detail")
	private String detail = null;

	public enum StatusEnum {
		pending("pending"), done("done");
		private String value;

		StatusEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}
	}

	@XmlElement(name = "status")
	@JsonProperty("status")
	private StatusEnum status = null;
	
	public TaskModel() {
		
	}

	/**
	 * Constructor using fields
	 * 
	 * @param id
	 * @param subject
	 * @param detail
	 * @param status
	 */
	public TaskModel(Integer id, String subject, String detail, StatusEnum status) {
		this.id = id;
		this.subject = subject;
		this.detail = detail;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TaskModel task = (TaskModel) o;
		return Objects.equals(id, task.id) && Objects.equals(subject, task.subject)
				&& Objects.equals(detail, task.detail) && Objects.equals(status, task.status);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, subject, detail, status);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Task {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
		sb.append("    detail: ").append(toIndentedString(detail)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}