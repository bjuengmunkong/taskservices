package com.scale360.assignment.taskservices.dao.impl;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import com.scale360.assignment.taskservices.dao.GenericDAO;
import com.scale360.assignment.taskservices.persistence.PersistenceManager;

public abstract class GenericDAOImpl<T, PK extends Serializable> implements GenericDAO<T, PK> {
	protected EntityManager em;

	protected Class<T> type;

	@SuppressWarnings("unchecked")
	public GenericDAOImpl() {
		em = PersistenceManager.getEntityManager();
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<T>) pt.getActualTypeArguments()[0];
	}

	@Override
	public T create(final T t) {
		this.em.persist(t);
		return t;
	}

	@Override
	public List<T> create(final List<T> t) {
		for (T entity : t) {
			this.em.persist(entity);
		}
		return t;
	}

	@Override
	public void remove(final T t) {
		this.em.remove(t);
	}

	@Override
	public void remove(final List<T> t) {
		for (T entity : t) {
			this.em.remove(entity);
		}
	}

	@Override
	public void delete(final PK id) {
		this.em.remove(this.em.getReference(type, id));
	}

	@Override
	public T find(final PK id) {
		return (T) this.em.find(type, id);
	}

	public List<T> findByExample(T exampleInstance, Class<T> objectClass) throws NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		CriteriaBuilder cb = this.em.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(objectClass);
		Root<T> r = cq.from(objectClass);
		Predicate p = cb.conjunction();
		Metamodel mm = em.getMetamodel();
		EntityType<T> et = mm.entity(objectClass);
		Set<Attribute<? super T, ?>> attrs = et.getAttributes();

		for (Attribute<? super T, ?> a : attrs) {
			String name = a.getName();
			String javaName = a.getJavaMember().getName();
			String getter = "get" + javaName.substring(0, 1).toUpperCase() + javaName.substring(1);

			Method m = objectClass.getMethod(getter, (Class<?>[]) null);
			if (m.invoke(exampleInstance, (Object[]) null) != null)
				p = cb.and(p, cb.equal(r.get(name), m.invoke(exampleInstance, (Object[]) null)));
		}
		cq.select(r).where(p);
		TypedQuery<T> query = em.createQuery(cq);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {
		Query query = this.em.createQuery("SELECT o FROM " + type.getSimpleName() + " o");
		return query.getResultList();
	}

	public List<T> findAll(Class<T> clazz, Order order, String... propertiesOrder) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		Root<T> root = cq.from(clazz);

		List<javax.persistence.criteria.Order> orders = new ArrayList<>();
		for (String propertyOrder : propertiesOrder) {
			if (order.isAscending()) {
				orders.add(cb.asc(root.get(propertyOrder)));
			} else {
				orders.add(cb.desc(root.get(propertyOrder)));
			}
		}
		cq.orderBy(orders);

		return em.createQuery(cq).getResultList();
	}

	@Override
	public T update(final T t) {
		T mergedEntity = this.em.merge(t);
		return mergedEntity;
	}

	@Override
	public List<T> update(final List<T> t) {
		List<T> updatedObjects = new ArrayList<T>();
		for (T entity : t) {
			updatedObjects.add(this.em.merge(entity));
		}
		return updatedObjects;
	}

	@Override
	public T save(final T t) {
		if (this.em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(t) == null) {
			return this.create(t);
		} else {
			return this.update(t);
		}
	}

	@Override
	public List<T> save(final List<T> t) {
		List<T> updatedObjects = new ArrayList<T>();
		for (T entity : t) {
			if (this.em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity) == null) {
				this.create(entity);
			} else {
				this.update(entity);
			}
		}
		return updatedObjects;
	}

}
