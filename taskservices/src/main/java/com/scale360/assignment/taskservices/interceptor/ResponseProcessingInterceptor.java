package com.scale360.assignment.taskservices.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.interception.PostProcessInterceptor;

import com.scale360.assignment.taskservices.persistence.PersistenceManager;

@Provider
@ServerInterceptor
public class ResponseProcessingInterceptor implements PostProcessInterceptor {
	
	private static Logger logger = Logger.getLogger(ResponseProcessingInterceptor.class);
	
	@Context
    private HttpServletRequest servletRequest;

	@Override
	public void postProcess(ServerResponse response) {
		logger.debug("post process interceptor");
		
		if (isValidResponseStatus(response.getStatus())) {
			// do not commit and close for http method is OPTIONS
			if (!HttpMethod.OPTIONS.equalsIgnoreCase(servletRequest.getMethod())) {
				logger.debug("ResponseStatus is VALID - PersistenceManager Commit and Close");
				PersistenceManager.commitAndClose();	
			}			
		} else {
			logger.debug("ResponseStatus is INVALID - PersistenceManager Rollback and Close");
			PersistenceManager.rollbackAndClose();
		}
	}

	private boolean isValidResponseStatus(int status) {
		if (Status.ACCEPTED.equals(Status.fromStatusCode(status))
				|| Status.OK.equals(Status.fromStatusCode(status))
				|| Status.CREATED.equals(Status.fromStatusCode(status))) {
			return true;
		}
		return false;
	}

}
