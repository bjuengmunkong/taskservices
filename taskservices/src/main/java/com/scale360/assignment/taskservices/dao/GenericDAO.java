package com.scale360.assignment.taskservices.dao;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface GenericDAO<T, PK extends Serializable>
{
    /**
     * Method that returns the number of entries from a table that meet some criteria (where clause params)
     *
     * @param params sql parameters
     * @return the number of records meeting the criteria
     */

    T create(T t);
    
    List<T> create(List<T> t);

    T find(PK id);
    
    List<T> findByExample(T exampleInstance, Class<T> objectClass) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;

    List<T> findAll();

    T update(T t);

    List<T> update(List<T> t);

    void remove(T t);

    void remove(List<T> t);

    void delete(PK id);
    
    T save(T t);
    
    List<T> save(List<T> t);
}
