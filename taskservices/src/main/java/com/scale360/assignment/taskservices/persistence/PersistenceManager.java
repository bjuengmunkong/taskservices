package com.scale360.assignment.taskservices.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {
	private static EntityManagerFactory factory;
	private static final ThreadLocal<EntityManager> entityManagerHolder = new ThreadLocal<EntityManager>();

	public static EntityManager getEntityManager() {
		return getEntityManager("taskPU");
	}

	private static EntityManager getEntityManager(String persistenceUnitName) {

		factory = Persistence.createEntityManagerFactory(persistenceUnitName);

		EntityManager em = entityManagerHolder.get();

		if (em == null) {
			em = factory.createEntityManager();
			em.getTransaction().begin();
			entityManagerHolder.set(em);

		} else {

			if (em.isOpen()) {

			} else {
				entityManagerHolder.remove();
				em = factory.createEntityManager();
				em.getTransaction().begin();
				entityManagerHolder.set(em);
			}

			if (!em.getTransaction().isActive()) {
				em.getTransaction().begin();
			}

		}
		return em;
	}

	public static void commitAndClose() {
		EntityManager em = entityManagerHolder.get();
		try {
			if (em != null) {
				try {
					if (em.getTransaction().isActive()) {
						em.getTransaction().commit();
					}
				} finally {
					em.close();
				}
			}
		} finally {
			entityManagerHolder.remove();
		}
	}

	public static void rollbackAndClose() {
		EntityManager em = entityManagerHolder.get();
		try {
			if (em != null) {
				try {
					em.getTransaction().rollback();
				} finally {
					em.close();
				}
			}
		} finally {
			entityManagerHolder.remove();
		}
	}
}
