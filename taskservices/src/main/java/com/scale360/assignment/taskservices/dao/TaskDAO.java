package com.scale360.assignment.taskservices.dao;

import java.io.Serializable;

import com.scale360.assignment.taskservices.persistence.entities.Task;

public interface TaskDAO extends GenericDAO<Task, Serializable> {
	

}
