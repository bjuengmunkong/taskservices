package com.scale360.assignment.taskservices.business;

import java.util.List;

import com.scale360.assignment.taskservices.model.TaskModel;

public interface TaskBusinessAction {
	/**
	 * Gets all tasks
	 * 
	 * @return all tasks in list
	 * @throws Exception
	 */
	public List<TaskModel> getTasks() throws Exception;
	
	/**
	 * Gets task by taskId
	 * 
	 * @param taskId
	 * @return task
	 * @throws Exception
	 */
	public TaskModel getTaskByTaskId(Integer taskId) throws Exception;
	
	/**
	 * Updates task status by taskId 
	 * 
	 * @param taskId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public TaskModel updateTaskStatusByTaskId(Integer taskId, String status) throws Exception;
	
	/**
	 * Removes task by taskId 
	 * 
	 * @param taskId
	 * @throws Exception
	 */
	public void deleteTaskByTaskId(Integer taskId) throws Exception;
	
	/**
	 * Updates task by task model
	 * 
	 * @param taskModel
	 * @return
	 * @throws Exception
	 */
	public TaskModel updateTask(TaskModel taskModel) throws Exception;
	
	/**
	 * Creates task by task model
	 * 
	 * @param taskModel
	 * @return
	 * @throws Exception
	 */
	public TaskModel createTask(TaskModel taskModel) throws Exception;
	
}
