package com.scale360.assignment.taskservices.model;

import org.junit.Assert;
import org.junit.Test;

public class TaskModelTest {

	@Test
	public void createTaskModelResponse_ShouldBeReturn_Status_Pending() {
		TaskModel tm = new TaskModel(1, "xxx", "yyy", TaskModel.StatusEnum.pending);
		Assert.assertEquals(Integer.valueOf(1), tm.getId());
		Assert.assertEquals("xxx", tm.getSubject());
		Assert.assertEquals("yyy", tm.getDetail());
		Assert.assertEquals("pending", tm.getStatus().toString());
	}

	@Test
	public void createTaskModelResponse_ShouldBeReturn_Status_Done() {
		TaskModel tm = new TaskModel(2, "xxx", "yyy", TaskModel.StatusEnum.done);
		Assert.assertEquals(Integer.valueOf(2), tm.getId());
		Assert.assertEquals("xxx", tm.getSubject());
		Assert.assertEquals("yyy", tm.getDetail());
		Assert.assertEquals("done", tm.getStatus().toString());
	}
}
