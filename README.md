#Task WebService 
It's created via RestEasy framework and uses swagger for the api document. 

see api document on swagger-ui using ${project.basedir}/doc/taskservices.yaml
___

##Instruction 

###Prerequisite
- setup MySQL database, using ${project.basedir}/doc/taskdb.sql
- JBoss EAP 6.x as a webServer  

###How to Compile 
- go to repository path
- run cmd -> mvn clean install -P ${profile} (default profile=local)	
** please make sure about maven and java jdk setup.

###How to Deploy 
- put the packing war file (taskservices-${env}.war) into the deploy path (it will be ${jboss_home}/standalone/deployments/)
- start web server and see war package has been deployed.   

___

###Software Architecture 
- java 1.8
- maven

___

###Software libraries and framework
- resteasy 
- hibernate
- mysql
- junit
- mockito

___

###Standard format 
- json
- xml

___

###WIP (work in progress)
- ~~design architecture~~
- ~~create task api service~~
- perform unit testing
- adjust source code
- fix issues

___

###API 
|       Description          		|          Api Service           	|  Http Method	|
|-----------------------------------|-----------------------------------|---------------|
| view all tasks in the list	    | /tasks                   			| GET			|
| view a signle task in the list	| /task/${taslId}          			| GET			|
| add a task to the list 			| /task           					| POST			|
| edit exiting task        			| /task/${taskId}             		| PUT			|
| set the task status           	| /task/${taslId}?status=${status}	| PUT			|
| delete a task from  the list   	| /task/${taskId} 					| DELETE		|


###Task Model
- id
- subject
- detail
- status (pending/done)